import React, { Component } from "react";
import ShowMovies from "./showmovies";
import AddDirector from "./addDirector";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";

class Directors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      items: [],
      directors: [],
      isLoaded: false,
      onAdd: false,
      popUp: false
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/directors/")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            items: result.map(item => item),
            directors: result.map(item => item)
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  delete = async e => {
    console.log(e.target.parentNode.id);
    let id = e.target.parentNode.id;
    await fetch("http://localhost:8080/api/directors/" + id, {
      method: "DELETE"
    });
    await this.componentDidMount();
  };

  searchBar = e => {
    let directors = [];
    let newdirectors = [];
    if (e.target.value !== "") {
      directors = this.state.directors;
      newdirectors = directors.filter(elem => {
        const lc = elem.name.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return lc.includes(filter);
      });
    } else if (e.target.value === "") {
      newdirectors = this.state.directors;
    }
    this.setState({
      items: newdirectors
    });
  };

  addForm = e => {
    this.setState({
      onAdd: !this.state.onAdd,
      id: e.target.parentNode.id
    });
  };

  addNewDirector = async name => {
    await fetch("http://localhost:8080/api/directors/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        Director: `${name}`
      })
    });
    await this.componentDidMount();
    this.setState({
      onAdd: false
    });
  };

  updateDirector = async name => {
    await fetch("http://localhost:8080/api/directors/" + this.state.id, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        Director: `${name}`
      })
    });
    await this.componentDidMount();
    this.setState({
      onAdd: false
    });
  };

  showPopUp = e => {
    console.log(e.target.id);
    this.setState({
      popUp: true,
      detailsId: e.target.id
    });
  };

  closePopUp = () => {
    this.setState({
      popUp: !this.state.popUp
    });
  };

  render() {
    const { error, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <React.Fragment>
          <div className="directors-container">
            <div className="addAndEdit">
              {this.state.onAdd && (
                <AddDirector
                  onAdd={this.addNewDirector}
                  onEditId={this.state.id}
                  onEdit={this.updateDirector}
                />
              )}
            </div>
            <div className="insertSearch">
              <input
                className="searchBar"
                type="text"
                onChange={this.searchBar}
                placeholder="  Search...."
              />
              <button className="addNewDirector" onClick={this.addForm}>
                Add
              </button>
            </div>

            <ul className="list">
              {items.map(item => (
                <li
                  key={item.directorId}
                  className="directorsName"
                  id={item.directorId}
                  onClick={this.showPopUp}
                >
                  {item.name}
                  <button className="delete" onClick={this.delete}>
                    X
                  </button>
                  <button className="Edit" onClick={this.addForm}>
                    Edit
                  </button>
                </li>
              ))}
            </ul>
            {this.state.popUp && (
              <ShowMovies
                movies={this.state.items.filter(
                  item => item.directorId == this.state.detailsId
                )}
                Id={this.state.Id}
                onClose={this.closePopUp}
              />
            )}
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Directors;
