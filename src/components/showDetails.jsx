import React, { Component } from "react";

class ShowDetails extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="detailsContainer">
          {this.props.movies.map(item => (
            <ul>
              <li className="details">{item.Title}</li>
              <li className="details">Rank: {item.Rank}</li>
              <li className="details">Description: {item.Description}</li>
              <li className="details">Runtime: {item.Runtime}</li>
              <li className="details">Genre: {item.Genre}</li>
              <li className="details">Rating: {item.Rating}</li>
              <li className="details">Metascore: {item.Metascore}</li>
              <li className="details">votes: {item.votes}</li>
              <li className="details">
                Gross Earning(Millions): {item.Gross_Earning_in_Mil}
              </li>
              <li className="details">Director: {item.Director}</li>
              <li className="details">Actor: {item.Actor}</li>
              <li className="details">Year: {item.Year}</li>

              <button className="close" onClick={this.props.onClose}>
                X
              </button>
            </ul>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default ShowDetails;
