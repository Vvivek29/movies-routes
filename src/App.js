import React, { Component } from "react";
import Directors from "./components/directors";
import Movies from "./components/movies";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import "./App.css";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <div className="container">
            <div className="main-container">
              <Route path="/directors" exact component={Directors} />

              <Link to="/directors" className="directors">
                Directors
              </Link>
              <Route path="/movies" exact component={Movies} />
              <Link to="/movies" className="movies">
                Movies
              </Link>
              <Route path="/" />
              <Link to="/" className="head">
                IMDb
              </Link>
            </div>
          </div>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
