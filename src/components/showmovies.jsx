import React, { Component } from "react";

class ShowMovies extends Component {
  state = {};
  render() {
    console.log(this.props.moviesList);
    return (
      <React.Fragment>
        <div className="moviesList">
          {this.props.movies.map(item => (
            <ul>
              <li className="movie">{item.Title[0]}</li>
              <li className="movie">{item.Title[1]}</li>
              <li className="movie">{item.Title[2]}</li>
              <li className="movie">{item.Title[3]}</li>
              <li className="movie">{item.Title[4]}</li>
              <button className="close" onClick={this.props.onClose}>
                X
              </button>
            </ul>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default ShowMovies;
