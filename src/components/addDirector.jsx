import React, { Component } from "react";

class AddDirector extends Component {
  state = {};

  handleSubmit = e => {
    if (!this.props.onEditId) {
      let name = e.target.parentNode.childNodes[0].childNodes[1].value;
      this.props.onAdd(name);
    } else {
      let name = e.target.parentNode.childNodes[0].childNodes[1].value;
      this.props.onEdit(name);
    }
  };

  render() {
    return (
      <React.Fragment>
        <div>
          <form className="addDirector">
            <label className="directorName">
              Director Name:
              <input
                className="input m-2"
                name="name"
                type="text"
                placeholder="Director name"
              />
            </label>
            <input
              type="button"
              className="btn btn-info m-2"
              value="Add"
              onClick={this.handleSubmit}
            />
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default AddDirector;
