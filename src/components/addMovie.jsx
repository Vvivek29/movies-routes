import React, { Component } from "react";

class AddMovies extends Component {
  state = {};

  handleSubmit = e => {
    if (!this.props.onEditId) {
      let movies = {
        Rank: `${e.target.parentNode.childNodes[0].childNodes[1].value}`,
        Title: `${e.target.parentNode.childNodes[1].childNodes[1].value}`,
        Description: `${e.target.parentNode.childNodes[2].childNodes[1].value}`,
        Runtime: `${e.target.parentNode.childNodes[3].childNodes[1].value}`,
        Genre: `${e.target.parentNode.childNodes[4].childNodes[1].value}`,
        Rating: `${e.target.parentNode.childNodes[5].childNodes[1].value}`,
        Metascore: `${e.target.parentNode.childNodes[6].childNodes[1].value}`,
        Votes: `${e.target.parentNode.childNodes[7].childNodes[1].value}`,
        Gross_Earning_in_Mil: `${
          e.target.parentNode.childNodes[8].childNodes[1].value
        }`,
        director_id: `${e.target.parentNode.childNodes[9].childNodes[1].value}`,
        Director: `${e.target.parentNode.childNodes[10].childNodes[1].value}`,
        Actor: `${e.target.parentNode.childNodes[11].childNodes[1].value}`,
        Year: `${e.target.parentNode.childNodes[12].childNodes[1].value}`
      };
      this.props.onAdd(movies);
    } else {
      console.log(e.target.parentNode.childNodes[0].childNodes[1].value);
      let movies = {
        Rank:
          `${e.target.parentNode.childNodes[0].childNodes[1].value}` !== ""
            ? `${e.target.parentNode.childNodes[0].childNodes[1].value}`
            : this.props.data[0].Rank,
        Title:
          `${e.target.parentNode.childNodes[1].childNodes[1].value}` !== ""
            ? `${e.target.parentNode.childNodes[1].childNodes[1].value}`
            : this.props.data[0].Title,
        Description: `${e.target.parentNode.childNodes[2].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[2].childNodes[1].value}`
          : this.props.data[0].Description,
        Runtime: `${e.target.parentNode.childNodes[3].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[3].childNodes[1].value}`
          : this.props.data[0].Runtime,
        Genre: `${e.target.parentNode.childNodes[4].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[4].childNodes[1].value}`
          : this.props.data[0].Genre,
        Rating: `${e.target.parentNode.childNodes[5].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[5].childNodes[1].value}`
          : this.props.data[0].Rating,
        Metascore: `${e.target.parentNode.childNodes[6].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[6].childNodes[1].value}`
          : this.props.data[0].Metascore,
        Votes: `${e.target.parentNode.childNodes[7].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[7].childNodes[1].value}`
          : this.props.data[0].votes,
        Gross_Earning_in_Mil: `${
          e.target.parentNode.childNodes[8].childNodes[1].value
        }`
          ? `${e.target.parentNode.childNodes[8].childNodes[1].value}`
          : this.props.data[0].Gross_Earning_in_Mil,
        director_id: `${e.target.parentNode.childNodes[9].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[9].childNodes[1].value}`
          : this.props.data[0].director_id,
        Director: `${e.target.parentNode.childNodes[10].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[10].childNodes[1].value}`
          : this.props.data[0].Director,
        Actor: `${e.target.parentNode.childNodes[11].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[11].childNodes[1].value}`
          : this.props.data[0].Actor,
        Year: `${e.target.parentNode.childNodes[12].childNodes[1].value}`
          ? `${e.target.parentNode.childNodes[12].childNodes[1].value}`
          : this.props.data[0].Year
      };
      this.props.onEdit(movies);
    }
  };

  render() {
    console.log(this.props.data);
    return (
      <React.Fragment>
        <div className="addContainer">
          <form className="addForm">
            <label>
              <input
                className="input m-2"
                name="Rank"
                type="text"
                placeholder="Rank"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Title"
                type="text"
                placeholder="Title"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Description"
                type="text"
                placeholder="Description"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Runtime"
                type="text"
                placeholder="Runtime"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Genre"
                type="text"
                placeholder="Genre"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Rating"
                type="text"
                placeholder="Rating"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Metascore"
                type="text"
                placeholder="Metascore"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Votes"
                type="text"
                placeholder="Votes"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Gross Earning"
                type="text"
                placeholder="Gross Earning"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="director_id"
                type="text"
                placeholder="Director Id"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Director"
                type="text"
                placeholder="Director"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Actor"
                type="text"
                placeholder="Actor"
              />
            </label>
            <label>
              <input
                className="input m-2"
                name="Year"
                type="text"
                placeholder="Year"
              />
            </label>
            <input
              type="button"
              className="btn btn-info m-2"
              value="Add"
              onClick={this.handleSubmit}
            />
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default AddMovies;
