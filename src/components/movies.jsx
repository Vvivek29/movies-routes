import React, { Component } from "react";
import ShowDetails from "./showDetails";
import AddMovies from "./addMovie";

class Movies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      movies: [],
      details: false,
      onAdd: false
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/movies/")
      .then(res => res.json())
      .then(result => {
        this.setState({
          isLoaded: true,
          items: result.map(item => item),
          movies: result.map(item => item)
        });
      });
  }

  searchBar = e => {
    let movies = [];
    let newMovies = [];
    if (e.target.value !== "") {
      movies = this.state.movies;
      newMovies = movies.filter(elem => {
        const lc = elem.Title.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return lc.includes(filter);
      });
    } else if (e.target.value === "") {
      newMovies = this.state.movies;
    }
    this.setState({
      items: newMovies
    });
  };

  moviesDetails = e => {
    this.setState({
      popUp: true,
      id: e.target.id
    });
  };

  addForm = e => {
    console.log(e.target.parentNode.id);
    this.setState({
      onAdd: !this.state.onAdd,
      idEdit: e.target.parentNode.id
    });
  };

  addNewMovies = async movies => {
    console.log(movies);
    await fetch("http://localhost:8080/api/movies/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },

      body: JSON.stringify(movies)
    });
    await this.componentDidMount();
    this.setState({
      onAdd: false
    });
  };

  updateMovies = async movies => {
    await fetch(`http://localhost:8080/api/movies/${this.state.idEdit}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(movies)
    });
    await this.componentDidMount();
    this.setState({
      onAdd: false
    });
  };

  delete = async e => {
    console.log(e.target.parentNode.id);
    let id = e.target.parentNode.id;
    await fetch("http://localhost:8080/api/movies/" + id, {
      method: "DELETE"
    });
    await this.componentDidMount();
  };

  closePopUp = () => {
    this.setState({
      popUp: !this.state.popUp
    });
  };
  render() {
    const { items } = this.state;

    return (
      <React.Fragment>
        <div className="movies-container">
          <div className="showdetails">
            {this.state.popUp && (
              <ShowDetails
                movies={this.state.items.filter(
                  item => item.id == this.state.id
                )}
                id={this.state.id}
                onClose={this.closePopUp}
              />
            )}
          </div>
          <div className="addMovies">
            {this.state.onAdd && (
              <AddMovies
                onAdd={this.addNewMovies}
                onEditId={this.state.idEdit}
                onEdit={this.updateMovies}
                data={this.state.items.filter(
                  elem => elem.id == this.state.idEdit
                )}
              />
            )}
          </div>
          <div className="insertSearch">
            <input
              className="searchBar"
              type="text"
              onChange={this.searchBar}
              placeholder="  Search...."
            />
            <button className="addNewMovie" onClick={this.addForm}>
              Add
            </button>
          </div>

          <ul className="list">
            {items.map(item => (
              <li
                key={item.id}
                onClick={this.moviesDetails}
                className="moviesName"
                id={item.id}
              >
                {item.Title}
                <button className="delete" onClick={this.delete}>
                  X
                </button>
                <button className="Edit" onClick={this.addForm}>
                  Edit
                </button>
              </li>
            ))}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default Movies;
